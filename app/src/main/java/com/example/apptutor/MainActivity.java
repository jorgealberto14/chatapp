package com.example.apptutor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    //Firebase
    private FirebaseAuth auth;

    //Google
    private GoogleApiClient googleApiClient;
    private SignInButton signInButton;
    public static  final int SIGN_IN_CODE = 777;

    //Creacion de variables
    TextView txtusuario;
    TextView txtcontraseña;
    MaterialButton btnEntrar;
    TextView registrarse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Variables de Firebase
        auth = FirebaseAuth.getInstance();

        txtusuario = findViewById(R.id.usuario);
        txtcontraseña = findViewById(R.id.contraseña);


        //Al hacer click en el boton
        btnEntrar = findViewById(R.id.iniciarSesion);
        btnEntrar.setOnClickListener(view -> {
            iniciarSesion();
        });

        //Metodo para pasar a la clase Registrar
        registrarse = findViewById(R.id.registrar);
        registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Registrar.class));
            }
        });

        //Metodos para Autenticar mediante Google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();
        signInButton = findViewById(R.id.registroGoogle);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent, SIGN_IN_CODE);
            }
        });

    }

    //Metodo para validar el inicio de sesion
    private void iniciarSesion() {
        String correo = txtusuario.getText().toString();
        String contraseña = txtcontraseña.getText().toString();

        if (TextUtils.isEmpty(correo)){
            txtusuario.setError("Ingrese un correo");
        }else if (TextUtils.isEmpty(contraseña)){
            Toast.makeText(MainActivity.this, "Ingrese una contraseña", Toast.LENGTH_SHORT).show();
            txtcontraseña.requestFocus();
        }else{
            auth.signInWithEmailAndPassword(correo, contraseña).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){
                        Toast.makeText(MainActivity.this, "Ingreso Exitoso", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(MainActivity.this, Registrar.class));
                    }else{
                        Log.w("TAG", "Error: ", task.getException() );
                    }
                }
            });
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SIGN_IN_CODE){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()){
            pasarDatos();
        }else {
            Toast.makeText(this, "No se pudo iniciar sesion", Toast.LENGTH_SHORT).show();
        }
    }

    private void pasarDatos() {
        Intent intent = new Intent(this, Registrar.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}