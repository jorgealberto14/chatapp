package com.example.apptutor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class Registrar extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    //Variables
    private EditText txtNombre;
    private EditText txtCorreo;
    private EditText txtContraseña;
    private Spinner txtRol;
    private EditText txtEdad;
    private MaterialButton btnRegister;

    //Variables de Firebase
    private FirebaseAuth auth;
    private FirebaseFirestore db;
    private String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        //Metodo para el combobox
        Spinner combobox = findViewById(R.id.rol);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.rol, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        combobox.setAdapter(adapter);
        combobox.setOnItemSelectedListener(this);

        //Asignamos el objeto a las variables
        txtNombre = findViewById(R.id.nombre);
        txtCorreo = findViewById(R.id.correo);
        txtContraseña = findViewById(R.id.contraseña);
        txtRol = findViewById(R.id.rol);

        txtEdad = findViewById(R.id.edad);
        btnRegister = findViewById(R.id.btnRegistrar);

        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();


        //Boton para registrar
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarUsuario();
            }
        });

        //Metodo para obtener los datos del registrar Google

    }

    //Metodo para guardar los datos a la base de datos.
    public void guardarUsuario(){
        String nombre = txtNombre.getText().toString();
        String correo = txtCorreo.getText().toString();
        String contraseña = txtContraseña.getText().toString();
        String rol = txtRol.getSelectedItem().toString();
        String edad = txtEdad.getText().toString();

        if (TextUtils.isEmpty(nombre)){
            txtNombre.setError("Ingrese un nombre");
            txtNombre.requestFocus();
        }else if (TextUtils.isEmpty(correo)){
            txtCorreo.setError("Ingrese un correo");
            txtCorreo.requestFocus();
        }else if (TextUtils.isEmpty(contraseña)){
            txtContraseña.setError("Ingrese una contraseña");
            txtContraseña.requestFocus();
        }else if (TextUtils.isEmpty(edad)){
            txtEdad.setError("Ingrese una edad");
            txtEdad.requestFocus();
        }

        auth.createUserWithEmailAndPassword(correo, contraseña).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    userID = auth.getCurrentUser().getUid();
                    DocumentReference documentReference = db.collection("usuarios").document(userID);

                    Map<String, Object> usuario = new HashMap<>();
                    usuario.put("Nombre", nombre);
                    usuario.put("Correo", correo);
                    usuario.put("Contraseña", contraseña);
                    usuario.put("rol", rol);
                    usuario.put("Edad", edad);

                    documentReference.set(usuario).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Log.d("TAG", "Datos Registrados" + userID);
                        }
                    });
                    Toast.makeText(Registrar.this, "Usuario registrado correctamente", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Registrar.this, MainActivity.class));
                }else {
                    Toast.makeText(Registrar.this, "Usuario no registrado" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String itemSeleccionado = adapterView.getItemAtPosition(i).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}